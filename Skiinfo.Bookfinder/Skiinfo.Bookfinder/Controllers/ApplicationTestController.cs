﻿using System;
using System.Linq;
using System.Web.Mvc;
using Skiinfo.Bookfinder.Models;

namespace Skiinfo.Bookfinder.Controllers
{
    public class ApplicationTestController : Controller
    {
        private bookfinderDBEntities db = new bookfinderDBEntities();

        public ActionResult Index(string searchString)
        {
            var books = from m in db.application_test
                         select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(s => s.title.Contains(searchString) || s.year.Contains(searchString) || s.description.Contains(searchString));
            }

            return View(books);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
