# Application Test #

This README contains all you need to know for the MNC test.

### General info ###
This repository contains all the info you need to get started on your MNC developer tests. Consider the following scenario:
You are working with us on your first day, and you get handed the following requirement. Since you had lots of other stuff to do on your first day, you have only a limited time to implement the first iteration - after this, you have to finish and *something* has to be committed in this repository.

* You are free to choose any programming language, database, framework et cetera you like - as long as we can get it running on our machines without having to pay for access to 3rd party frameworks . We all have at least average development and google skills, so we are able to  understand your solution regardless of the language.
* If you don't finish, please assume that other team members with skills similar to yours will continue to work on/with your code
* You get the chance of presenting/explaining your code to these other team members after the one hour coding time. This is also the time to explain shortcuts you might need to take due to the time constraint.
* Like the real world scenario, the Designer and anyone from the Content Team will be unavailable
* Like in the real world scenario, the Product Manager is there to answer your questions. Feel free to ask him as many questions as you like, this does not affect the rating of your code
* For your convenience, we added a draft of our style guide to this repository
* For your convenience, we converted the CSV to SQL and Mongo Dumps for you. Feel free to use any format you like.


### The requirement ###
Assume that the Mountain News Corporation has discovered a new revenue stream: The Skiinfo BookFinder.

- The BookFinder is a website which allows the user to search for books. An end user can type any keyword in a search box, and sees the result of his search.
- Our designer has created a mockup for you, illustrating how the BookFinder should look. You can find the mockup in this repository.
- Our content team has provided an excel sheet, containing sample book data. You can find the sheet in this repository.

For your task, it's sufficient that you provide the website and some kind of backend serving the page and the data. Your solution should keep the "big picture" or roadmap (see below) in mind - in a real world scenario, those would also be things you and your team would have to implement eventually.

### Roadmap ###
The Skiinfo Bookfinder is going to have the following features in the future. You DO NOT need to implement them for this task. However, as mentioned before, these would be logical next steps for you and your team to work on, so you can keep them in mind while designing and implementing your solution.

- **Amazon Partner Link:** The user clicks on any of the books and is redirected to a link from Amazon. They pay the book, we get provision. That's how the Bookfinder makes money.
- **Tracking:** We want to track each link and search term on the Skiinfo Bookfinder to find out which books the users are most interested in.
- **Content Management System**: Our editors want to enter new books and edit or delete existing ones
- **Responsive:** We want to serve the Bookfinder on mobile devices in the future.
- **App:** We want to serve the Bookfinder, at least the data, also in an app